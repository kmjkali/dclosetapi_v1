package com.example.DCloset.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor


public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        if (tokenFull== null || !tokenFull.startsWith("Bearer ")) {
            chain.doFilter(request,response);
            return;
        }
        token = tokenFull.split("")[1].trim();
        if (!jwtTokenProvider.validateToken(token)) {
            chain.doFilter(request,response);
            return;
        }
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request,response);

    }


}
