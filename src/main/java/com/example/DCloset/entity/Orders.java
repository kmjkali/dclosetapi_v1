package com.example.DCloset.entity;

import com.example.DCloset.enums.MemberGrade;
import com.example.DCloset.enums.OrderStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId")
    private Member member;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "goodsId")
    private Goods goods;


    @Column(nullable = false)
    private LocalDate orderDate;

    @Column(nullable = false)
    private LocalDate desiredDate;

    @Column(nullable = false)
    private LocalDate deadlineDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private OrderStatus orderStatus;



}
