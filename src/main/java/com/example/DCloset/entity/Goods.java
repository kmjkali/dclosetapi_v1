package com.example.DCloset.entity;

import com.example.DCloset.enums.GoodsSize;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Setter
@Getter
public class Goods {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime productCreateDate;

    @Column(nullable = false,length = 20)
    private String productName;

    @Column(nullable = false)
    private Integer productCode;

    @Column(nullable = false,columnDefinition = "text")
    private String productInfo;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private GoodsSize goodsSize;

    @Column(nullable = false, length = 10)
    private String productColor;

    @Column(nullable = false)

    private String productMainImage;
    @Column(nullable = false)

    private String productSubImage1;
    @Column(nullable = false)

    private String productSubImage2;
    @Column(nullable = false)

    private String productSubImage3;

    @Column(nullable = false)
    private Boolean YnMembership;

    @Column(nullable = false)
    private Boolean YnOffline;

    @Column(nullable = false)
    private Boolean YnFree;

    @Column(nullable = false)
    private Boolean YnOneDay;

    @Column(nullable = false)
    private Boolean YnPost;


}
