package com.example.DCloset.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.query.Order;

import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Delivery {
    //order에연결


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ordersID")
    private Orders orders;

    @Column(nullable = false)
    private LocalDate deliveryDate;

    @Column(nullable = false,length = 30)
    private String deliveryNumber;


}
