package com.example.DCloset.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class SendBack {
    //order 에 연결

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ordersID")
    private Orders orders;

    @Column(nullable = false)
    private LocalDate sendBackDate;

    @Column(nullable = false,length = 30)
    private String sendBackNumber;



}
