package com.example.DCloset.controller;
import com.example.DCloset.entity.Orders;
import com.example.DCloset.model.sendback.SendBackCreateRequest;
import com.example.DCloset.model.sendback.SendBackItem;
import com.example.DCloset.model.sendback.SendBackResponse;
import com.example.DCloset.service.OrderService;
import com.example.DCloset.service.SendBackService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/turn")
@CrossOrigin(origins = "*")
public class SendBackController {

    private final SendBackService sendBackService;
    private final OrderService orderService;

    //반송 데이터 등록하기
    @PostMapping("/new/order-id/{orderId}")
    public String setReturn(@PathVariable long orderId,@RequestBody SendBackCreateRequest request) {

        Orders orders = orderService.getOrders(orderId);

        sendBackService.setReturn(orders,request);
        return "ok";

    }
    //반송 데이터 리스트 전체 불러오기
    @GetMapping("/all")
    public List<SendBackItem> getList() {

        return sendBackService.getSendBackList();
    }
    //반송 데이터 불러오기
    @GetMapping("/detail/id/{id}")
    public SendBackResponse getSendBack(@PathVariable long id) {

        return sendBackService.getSendBack(id);

    }
    //반송 데이터 수정하기
    @PutMapping("/send_back/id/{id}")
    public String putSendBackChange(@PathVariable long id,@RequestBody SendBackCreateRequest request){

        sendBackService.putSendBackChange(id,request);

        return "ok";

    }
    //반송 데이터 삭제하기
    @DeleteMapping("/delete/{id}")

    public String delSendBack(@PathVariable long id){

        sendBackService.delSendBack(id);

        return "ok";


    }


}
