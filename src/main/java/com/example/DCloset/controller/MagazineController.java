package com.example.DCloset.controller;

import com.example.DCloset.entity.Member;
import com.example.DCloset.model.magazine.MagazineChangeRequest;
import com.example.DCloset.model.magazine.MagazineCreateRequest;
import com.example.DCloset.model.magazine.MagazineItem;
import com.example.DCloset.model.magazine.MagazineResponse;
import com.example.DCloset.service.MagazineService;
import com.example.DCloset.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/magazine")
@CrossOrigin(origins = "*")
public class MagazineController {
    private final MagazineService magazineService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    public String setMagazine(@PathVariable long memberId, MagazineCreateRequest request) {
        Member member = memberService.getMember(memberId);
        magazineService.setMagazine(member, request);
        return "OK";
    }

    @GetMapping("/all")
    public List<MagazineItem> getMagazines() {
        return magazineService.getMagazines();
    }

    @GetMapping("/detail/id/{id}")
    public MagazineResponse getMagazine(@PathVariable long id) {
        return magazineService.getMagazine(id);
    }

    @PutMapping("/change/id/{id}")
    public String putMagazine(@PathVariable long id, @RequestBody MagazineChangeRequest request) {
        magazineService.putMagazine(id, request);
        return "OK";
    }
}
