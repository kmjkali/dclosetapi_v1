package com.example.DCloset.controller;

import com.example.DCloset.entity.Member;
import com.example.DCloset.model.charge.ChargeCreateRequest;
import com.example.DCloset.model.charge.ChargeInfoItem;
import com.example.DCloset.model.charge.ChargeChangeRequest;
import com.example.DCloset.model.charge.ChargeResponse;
import com.example.DCloset.service.ChargeService;
import com.example.DCloset.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/charge")
@CrossOrigin(origins = "*")
public class ChargeController {

    private final ChargeService chargeService;
    private final MemberService memberService;

    //회원아이디를 받아서 결제 등록
    @PostMapping("/new/member-id/{memberId}")
    public String setCharge(@PathVariable long memberId,@RequestBody ChargeCreateRequest request){
        Member member =memberService.getMember(memberId);
        chargeService.setCharge(member,request);
        return "ok";

    }
    @GetMapping("/all")
    public List<ChargeInfoItem> getList(){
       return chargeService.getChargeList();

    }
    @GetMapping("/detail/{id}")
    public ChargeResponse getCharge(@PathVariable long id){
        return chargeService.getCharge(id);

    }
    @PutMapping("pay_info/{id}")
    public String putChargeChangeRequest(@PathVariable long id, ChargeChangeRequest request){


        chargeService.putChargeChangeRequest(id, request);
        return "ok";


    }


}
