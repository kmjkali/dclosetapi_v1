package com.example.DCloset.controller;

import com.example.DCloset.model.member.*;

import com.example.DCloset.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
@CrossOrigin(origins = "*")
public class MemberController {
    private final MemberService memberService;

    // 회원 가입 시 아이디 중복 검사 기능
    @GetMapping("/username/check")
    public MemberDupCheckResponse getMemberIdDubCheck(@RequestParam(name = "username") String username) {
        return memberService.getMemberIdDubCheck(username);
    }

    // 회원 가입 기능
    @PostMapping("/new")
    public String setMember (@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMember(request);
        return "OK";
    }

    // 전체 회원 리스트 조회
    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    // 회원 id로 조회
    @PutMapping("/membership/member-id/{memberId}")
    public String joinMembership(@PathVariable long memberId, @RequestBody Membership membership) {
        memberService.joinMembership(memberId, membership);

        return "OK";
    }

    // 회원 탈퇴 신청 ( 회원 등급 비회원으로 변경 )
    @PutMapping("/member-withdrawal/member-id/{memberId}")
    public String memberWithdrawal(@PathVariable long memberId) {
        memberService.memberWithdrawal(memberId);

        return "OK";
    }

    // 회원 상세 조회
    @GetMapping("/detail/{id}")
    public MemberResponse getResponse(@PathVariable long id) {
        return memberService.getResponse(id);
    }
}
