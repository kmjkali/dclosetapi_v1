package com.example.DCloset.controller;

import com.example.DCloset.entity.Member;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinChangeRequest;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinCreateRequest;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinItem;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinResponse;
import com.example.DCloset.service.MemberService;
import com.example.DCloset.service.NoticeBulletinService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/v1/NoticeBulletin")

public class NoticeBulletinController {

    private final NoticeBulletinService noticeBulletinService;
    private final MemberService memberService;

    //등록 C -
    @PostMapping("/new/member-id/{memberId}")
    public String setNotice(@PathVariable long memberId, @RequestBody NoticeBulletinCreateRequest request){
        Member member = memberService.getMember(memberId);
        noticeBulletinService.setNotice(member,request);
        return "ok";
    }
    //공지사항 리스트 불러오기
    @GetMapping("/list/all")
    public List<NoticeBulletinItem> getNotices (){
        return noticeBulletinService.getNotices();
    }
    //공지사항 상세확인
    @GetMapping("/detail/{id}")
    public NoticeBulletinResponse getNotice(@PathVariable long id) {
        return noticeBulletinService.getNotice(id);
    }

    //공지사항 수정하기
    @PutMapping("//{id}")
    public String NoticeBulletinChangeRequest (@PathVariable long id,@RequestBody NoticeBulletinChangeRequest request){
        noticeBulletinService.putNotice(id, request);

        return "ok";
    }

    //공지사항 삭제하기
    @DeleteMapping("/delete/{id}")
    public String delNotice (long id) {
        noticeBulletinService.delNotice(id);
        return "ok";
    }



}
