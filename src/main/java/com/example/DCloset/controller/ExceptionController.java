package com.example.DCloset.controller;


import com.example.DCloset.exception.CRiderMissingException;
import com.example.DCloset.model.generic.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//보안적인 문제가 터졌을떄 메세지를 처리해주는 컨트롤러
@RestController
@RequestMapping("/exception")
public class ExceptionController {


    //접근에 대한 거부 ..
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {throw new CRiderMissingException();
    }

    @GetMapping("/entry-point")

    public CommonResult accessEntryPoint() {throw new CRiderMissingException();}


}
