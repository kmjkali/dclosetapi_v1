package com.example.DCloset.controller;

import com.example.DCloset.model.order.OrderResponse;
import com.example.DCloset.service.GoodsService;
import com.example.DCloset.service.MemberService;
import com.example.DCloset.entity.Member;
import com.example.DCloset.entity.Goods;
import com.example.DCloset.model.order.OrderItem;
import com.example.DCloset.model.order.OrderRequest;
import com.example.DCloset.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/v1/order")
public class OrderController {
    private final OrderService orderService;
    private final MemberService memberService;
    private final GoodsService goodsService;

    //주문 등록하기
    @PostMapping("/new/member-id/{memberId}/goods-id/{goodsId}")
    public String setOrder(@PathVariable long memberId, @PathVariable long goodsId, @RequestBody OrderRequest orderRequest) {
            Member member = memberService.getMember(memberId);
        Goods goods = goodsService.getGood(goodsId);
        orderService.setOrder(member, goods , orderRequest);

        return "OK";
    }
    //주문 리스트 가져오기
    @GetMapping("/all")
    public List<OrderItem> getOrderList() {
        return orderService.getOrderList();

    }

    //주문 낱개  가져오기
    @GetMapping("/detail/order-id/{id}")
    public OrderResponse getOrder(@PathVariable long id) {
        return orderService.getOrder(id);
    }

    //회원별 주문 정보 조회
    @GetMapping("/detail/member-id/{memberId}")
    public OrderResponse getMember(@PathVariable long memberId){
        return orderService.getMemberOrder(memberId);
    }

    //주문 취소 기능 (삭제  = 주문을 삭제하고 주문할수 없는 상품입니다. 라고 보여준다)

    @DeleteMapping("/delete/id/{id}")
    public String delOrder(@PathVariable long id){
        orderService.delOrder(id);
        return "ok";
    }



}
