package com.example.DCloset.controller;


import com.example.DCloset.entity.Orders;
import com.example.DCloset.model.delivery.DeliveryCreateRequest;
import com.example.DCloset.model.delivery.DeliveryItem;
import com.example.DCloset.model.delivery.DeliveryResponse;
import com.example.DCloset.service.DeliveryService;
import com.example.DCloset.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/DLVY")
@CrossOrigin(origins = "*")
public class DeliveryController {

     private final OrderService orderService;
     private final DeliveryService deliveryService;


     @PostMapping("/new/orders-id/{ordersId}")
    public String setDelivery(long ordersId,@RequestBody DeliveryCreateRequest request){

         Orders orders = orderService.getOrders(ordersId);
         deliveryService.setDelivery(orders,request);

         return "ok";
     }
     @GetMapping("/all")
    public List<DeliveryItem> getList() {
         return deliveryService.getDeliveryList();
     }

     @GetMapping("/detail/{id}")
    public DeliveryResponse getDelivery (@PathVariable long id) {
         return deliveryService.getDelivery(id);
     }

     @PutMapping("/delivery/{id}")
    public String putDeliveryChange(@PathVariable long id, @RequestBody DeliveryCreateRequest request){

         deliveryService.putDeliveryChange(id,request);

         return "ok";
     }





}
