package com.example.DCloset.repository;

import com.example.DCloset.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByUsername(String username);

    Optional<Member> findByUsernameAndPassword(String username,String password);

    Optional<Member> findByUsername(String username);
}
