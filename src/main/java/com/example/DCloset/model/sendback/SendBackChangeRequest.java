package com.example.DCloset.model.sendback;


import com.example.DCloset.entity.Orders;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class SendBackChangeRequest {

    private Orders orders;


    private LocalDate sendBackDate;
    private String sendBackNumber;

}
