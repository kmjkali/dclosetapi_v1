package com.example.DCloset.model.sendback;


import com.example.DCloset.entity.Orders;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class SendBackResponse {


    private Long id;
    private Orders orders;
    private LocalDate sendBackDate;
    private String sendBackNumber;


}
