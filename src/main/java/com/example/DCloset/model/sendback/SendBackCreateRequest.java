package com.example.DCloset.model.sendback;

import com.example.DCloset.entity.Orders;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class SendBackCreateRequest {

    private LocalDate sendBackDate;
    private String sendBackNumber;
}
