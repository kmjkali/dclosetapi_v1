package com.example.DCloset.model.order;

import com.example.DCloset.entity.Delivery;
import com.example.DCloset.entity.Goods;
import com.example.DCloset.entity.Member;
import com.example.DCloset.entity.SendBack;
import com.example.DCloset.enums.MemberGrade;
import com.example.DCloset.enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderItem {
    private Long id;
    private Member member;
    private Goods goods;

    private LocalDate orderDate;
    private LocalDate desiredDate;
    private LocalDate deadlineDate;
    private OrderStatus orderStatus;

}
