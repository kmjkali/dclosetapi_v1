package com.example.DCloset.model.order;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderRequest {

    private LocalDate desiredDate;
}
