package com.example.DCloset.model.goods;

import com.example.DCloset.enums.GoodsSize;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class GoodsChangeRequest {

    private String productName;
    private Integer productCode;
    private String productInfo;
    private String productColor;
    private GoodsSize goodsSize;

    private Boolean YnMembership;
    private Boolean YnOffline;
    private Boolean YnFree;
    private Boolean YnOneDay;
    private Boolean YnPost;

    private String productMainImage;
    private String productSubImage1;
    private String productSubImage2;
    private String productSubImage3;

}
