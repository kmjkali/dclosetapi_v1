package com.example.DCloset.model.generic;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private List<T> list;
    private Long totalCount; // 총 데이터 개수
    private Integer totalPage; // 총 페이지 개수 일반 적으로 1
    private Integer currentPage; // 현재 페이지 번호 일반 적으로 1
}
