package com.example.DCloset.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String username;
    private String memberName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String memberAddress;
    private String memberDetailedAddress;
    private Integer postCode;
    private LocalDateTime subscriptDate;
    private Boolean NYPersonalInfo;
    private Boolean NYMarketing;
    private String memberGrade;
    private String payWay;
    private String payInfo;
    private String payDay;
}
