package com.example.DCloset.model.noticeBulletin;

import com.example.DCloset.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class NoticeBulletinItem {

    private Long id;
    private Member member;
    private LocalDate noticeCreateDate;
    private String noticeTitle;
    private String noticeContent;
}
