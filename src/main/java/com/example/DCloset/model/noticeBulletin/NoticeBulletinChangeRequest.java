package com.example.DCloset.model.noticeBulletin;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class NoticeBulletinChangeRequest {

    private LocalDate noticeCreateDate;
    private String noticeTitle;
    private String noticeContent;
}
