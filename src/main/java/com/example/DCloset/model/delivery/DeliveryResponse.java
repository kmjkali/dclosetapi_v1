package com.example.DCloset.model.delivery;


import com.example.DCloset.entity.Orders;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DeliveryResponse {

    private Long id;

    private Long orderId;

    private LocalDate deliveryDate;

    private String deliveryNumber;

}
