package com.example.DCloset.model.delivery;


import com.example.DCloset.entity.Orders;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DeliveryChangeRequest {

    private Orders orders;

    private LocalDate deliveryDate;
    private String deliveryNumber;



}
