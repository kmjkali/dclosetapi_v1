package com.example.DCloset.exception;

public class CMoneyOutOverException extends RuntimeException{
    public CMoneyOutOverException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMoneyOutOverException(String msg) {
        super(msg);
    }

    public CMoneyOutOverException() {
        super();
    }
}
