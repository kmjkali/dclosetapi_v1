package com.example.DCloset.service;

import com.example.DCloset.entity.Goods;
import com.example.DCloset.entity.Member;
import com.example.DCloset.repository.OrderRepository;
import com.example.DCloset.entity.Orders;
import com.example.DCloset.enums.OrderStatus;
import com.example.DCloset.model.order.OrderItem;
import com.example.DCloset.model.order.OrderRequest;
import com.example.DCloset.model.order.OrderResponse;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    public Orders getOrders(long id) {return orderRepository.findById(id).orElseThrow();}

    //주문정보 등록
    public void setOrder(Member member, Goods goods, OrderRequest orderRequest) {
        Orders addData = new Orders();
        LocalDate deadLineDate = orderRequest.getDesiredDate().plusDays(10);
        addData.setMember(member);
        addData.setGoods(goods);
        addData.setOrderDate(LocalDate.now());
        addData.setDesiredDate(orderRequest.getDesiredDate());
        addData.setDeadlineDate(deadLineDate);
        addData.setOrderStatus(OrderStatus.RECEIVE);

        orderRepository.save(addData);
    }


    //주문 정보 전체 조회
    public List<OrderItem> getOrderList() {
        List<Orders> originList = orderRepository.findAll();
        List<OrderItem> result = new LinkedList<>();

        for(Orders orders : originList) {
            OrderItem addItem = new OrderItem();

            addItem.setMember(orders.getMember());
            addItem.setGoods(orders.getGoods());
            addItem.setId(orders.getId());
            addItem.setOrderDate(orders.getOrderDate());
            addItem.setDesiredDate(orders.getDesiredDate());
            addItem.setDeadlineDate(orders.getDeadlineDate());
            addItem.setOrderStatus(orders.getOrderStatus());


            result.add(addItem);
        }

        return result;
    }

    //주문정보 주문 아이디 별 조회

    public OrderResponse getOrder(long id) {
        Orders originData = orderRepository.findById(id).orElseThrow();
        OrderResponse response = new OrderResponse();

        response.setGoods(originData.getGoods());
        response.setMember(originData.getMember());
        response.setId(originData.getId());
        response.setOrderDate(originData.getOrderDate());
        response.setDesiredDate(originData.getDesiredDate());
        response.setDeadlineDate(originData.getDeadlineDate());
        response.setOrderStatus(originData.getOrderStatus());

        return response;
    }

    //주문 정보 회원 아이디별 조회
    //단수R이 아니라 리스트로 구현해보자.

    public OrderResponse getMemberOrder(long memberId){
        // 고쳐야됨
        Orders originData = orderRepository.findById(memberId).orElseThrow();
        OrderResponse response = new OrderResponse();

        response.setMember(originData.getMember());
        response.setGoods(originData.getGoods());
        response.setId(originData.getId());
        response.setOrderDate(originData.getOrderDate());
        response.setDesiredDate(originData.getDesiredDate());
        response.setDeadlineDate(originData.getDeadlineDate());
        response.setOrderStatus(originData.getOrderStatus());

        return response;
    }
    //주문상태를 변경할수 있는 기능





    //주문 취소 기능 (삭제  = 주문을 삭제하고 주문할수 없는 상품입니다. 라고 보여준다)

    public void delOrder(long id){
        orderRepository.deleteById(id);
    }


}
