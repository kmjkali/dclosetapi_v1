package com.example.DCloset.service;


import com.example.DCloset.entity.Delivery;
import com.example.DCloset.entity.Orders;
import com.example.DCloset.model.delivery.DeliveryCreateRequest;
import com.example.DCloset.model.delivery.DeliveryItem;
import com.example.DCloset.model.delivery.DeliveryResponse;
import com.example.DCloset.repository.DeliveryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryService {

    private final DeliveryRepository deliveryRepository;

    public void setDelivery(Orders orders,DeliveryCreateRequest request) {

        Delivery addData = new Delivery();

        addData.setDeliveryDate(LocalDate.now());
        addData.setDeliveryNumber(request.getDeliveryNumber());
        addData.setOrders(orders);


        deliveryRepository.save(addData);

    }

    public List<DeliveryItem> getDeliveryList() {

        List<Delivery> originList = deliveryRepository.findAll();
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery:originList) {
            DeliveryItem addList = new DeliveryItem();
            addList.setDeliveryDate(delivery.getDeliveryDate());
            addList.setDeliveryNumber(delivery.getDeliveryNumber());
            addList.setOrderId(delivery.getOrders().getId());

            result.add(addList);
        }

        return result;


    }


    public DeliveryResponse getDelivery(long id){

        Delivery originData = deliveryRepository.findById(id).orElseThrow();

        DeliveryResponse response = new DeliveryResponse();

        response.setId(originData.getId());
        response.setDeliveryDate(originData.getDeliveryDate());
        response.setDeliveryNumber(originData.getDeliveryNumber());
        response.setOrderId(originData.getOrders().getId());
        return response;

    }

    public void  putDeliveryChange(long id, DeliveryCreateRequest request) {

        Delivery originData = deliveryRepository.findById(id).orElseThrow();
        originData.setDeliveryNumber(request.getDeliveryNumber());

        deliveryRepository.save(originData);
    }






    public void delDelivery(long id) {
        deliveryRepository.deleteById(id);
    }


}