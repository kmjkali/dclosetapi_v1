package com.example.DCloset.service;


import com.example.DCloset.entity.Orders;
import com.example.DCloset.entity.SendBack;
import com.example.DCloset.model.sendback.SendBackCreateRequest;
import com.example.DCloset.model.sendback.SendBackItem;
import com.example.DCloset.model.sendback.SendBackResponse;
import com.example.DCloset.repository.SendBackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SendBackService {

    private final SendBackRepository sendBackRepository;


    public void setReturn(Orders orders,SendBackCreateRequest request){

        SendBack addData = new SendBack();


        addData.setSendBackDate(request.getSendBackDate());
        addData.setSendBackNumber(request.getSendBackNumber());
        addData.setOrders(orders);





        sendBackRepository.save(addData);
    }

    public List<SendBackItem> getSendBackList(){

        List<SendBack> originList = sendBackRepository.findAll();
        List<SendBackItem> result = new LinkedList<>();

        for(SendBack sendBack:originList) {
            SendBackItem addList = new SendBackItem();
            addList.setSendBackDate(sendBack.getSendBackDate());
            addList.setSendBackNumber(sendBack.getSendBackNumber());
            addList.setOrders(sendBack.getOrders());

            result.add(addList);
        }

        return result;

    }

    public SendBackResponse getSendBack(long id) {

        SendBack originData =  sendBackRepository.findById(id).orElseThrow();

        SendBackResponse response = new SendBackResponse();

        response.setId(originData.getId());
        response.setSendBackDate(originData.getSendBackDate());
        response.setSendBackNumber(originData.getSendBackNumber());
        response.setOrders(originData.getOrders());

        return response;


    }

    public void putSendBackChange(long id, SendBackCreateRequest request) {

        SendBack originData = sendBackRepository.findById(id).orElseThrow();

        originData.setSendBackNumber(request.getSendBackNumber());
        originData.setSendBackDate(request.getSendBackDate());


        sendBackRepository.save(originData);


    }

    public void delSendBack(long id) {
        sendBackRepository.deleteById(id);
    }






}
