package com.example.DCloset.service;

import com.example.DCloset.entity.Member;
import com.example.DCloset.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService {

    //회원 레포랑 같이 일한다.
    private final MemberRepository memberRepository;

    @Override
    //아이디를 가지고 우리 회원이 맞는지 어떻게 확인할것이냐?
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{

        Member member = memberRepository.findByUsername(username).orElseThrow();

        return member;
    }
}
