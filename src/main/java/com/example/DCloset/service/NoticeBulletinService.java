package com.example.DCloset.service;


import com.example.DCloset.entity.Member;
import com.example.DCloset.entity.NoticeBulletin;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinChangeRequest;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinCreateRequest;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinItem;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinResponse;
import com.example.DCloset.repository.NoticeBulletinRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;


//공지사항에  클릭 조회수 기능을 넣어 줘야한다.
@Service
@RequiredArgsConstructor

public class NoticeBulletinService {

    private final NoticeBulletinRepository noticeBulletinRepository;

    //공지사항 C등록
    public void setNotice(Member member, NoticeBulletinCreateRequest request) {
        NoticeBulletin addData = new NoticeBulletin();
        addData.setMember(member);
        addData.setNoticeCreateDate(LocalDate.now());
        addData.setNoticeTitle(request.getNoticeTitle());
        addData.setNoticeContent(request.getNoticeContent());

        noticeBulletinRepository.save(addData);

    }


    //공지사항  전체 리스트 찾기 (복수R)

    public List<NoticeBulletinItem> getNotices () {
        List<NoticeBulletin> origrinList = noticeBulletinRepository.findAll();
        List<NoticeBulletinItem> result = new LinkedList<>();

        for(NoticeBulletin noticeBulletin:origrinList) {

            NoticeBulletinItem addItem = new NoticeBulletinItem();
            addItem.setId(noticeBulletin.getId());
            addItem.setMember(noticeBulletin.getMember());
            addItem.setNoticeCreateDate(LocalDate.now());
            addItem.setNoticeTitle(noticeBulletin.getNoticeTitle());
            addItem.setNoticeContent(noticeBulletin.getNoticeContent());

            result.add(addItem);


        }
        return result;
    }


    //공지사항 단수 R   id로 찾기
    public NoticeBulletinResponse getNotice (long id) {

        NoticeBulletin originData = noticeBulletinRepository.findById(id).orElseThrow();
        NoticeBulletinResponse response = new NoticeBulletinResponse();
        response.setId(originData.getId());
        response.setNoticeCreateDate(LocalDate.now());
        response.setMember(originData.getMember());
        response.setNoticeTitle(originData.getNoticeTitle());
        response.setNoticeContent(originData.getNoticeContent());

        return response;

    }


    //공지사항  id로 수정하기 기능

    public void putNotice(long id, NoticeBulletinChangeRequest request){
        NoticeBulletin originData = noticeBulletinRepository.findById(id).orElseThrow();
        originData.setNoticeCreateDate(LocalDate.now());
        originData.setNoticeTitle(request.getNoticeTitle());
        originData.setNoticeContent(request.getNoticeContent());

        noticeBulletinRepository.save(originData);


    }



    //공지사항  삭제기능 (삭제가 아니라 비활성화 기능? 그건 나중에 생각하고 우선 삭제 )

    public void delNotice (long id){
        noticeBulletinRepository.deleteById(id);


    }


}
